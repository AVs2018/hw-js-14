localStorage.getItem('theme') ? false : localStorage.setItem('theme', 'oldTheme');

window.addEventListener('load', () => {

    let currentTheme = localStorage.getItem('theme');

    const oldTheme = {
        NavBg : '#35444F',
        backgroundC : '#fff',
        btnC : 'rgb(226, 46, 196)'
    }

    const newTheme = {
        NavBg : 'rgb(226, 46, 196)',
        backgroundC : '#ef9ee1',
        btnC : 'rgb(226, 46, 196)'
    }
    currentTheme === 'oldTheme' ? applyStyles(oldTheme) : applyStyles(newTheme);

    document.querySelector('.theme-btn').addEventListener('click', changeTheme);

    function changeTheme() {
        let currTheme = localStorage.getItem('theme');
        if (currTheme === 'oldTheme') {
            localStorage.setItem('theme', 'newTheme');
            applyStyles(newTheme);
        } else {
            localStorage.setItem('theme', 'oldTheme');
            applyStyles(oldTheme);
        }
    }

    function applyStyles(themeName) {
        document.querySelector('.nav_top').style.backgroundColor = themeName.NavBg;
        document.querySelector('.bottom').style.backgroundColor = themeName.NavBg;
        document.querySelector('.theme-btn').style.background = themeName.btnC;
        document.querySelector("body").style.background = themeName.backgroundC;
    }
})










